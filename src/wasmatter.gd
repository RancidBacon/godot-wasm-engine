extends SceneTree

var wasm = preload("res://addons/wasm-engine/WasmEngine.gd") # TODO: Handle properly.


const MAX_EXPORTS_TO_DUMP: int = 20
const MAX_IMPORTS_TO_DUMP: int = MAX_EXPORTS_TO_DUMP

func dump_info(the_module):

    print(" ")
    print("Imports found: %d" % the_module.imports.size())
    print("Exports found: %d" % the_module.exports.size())

    for index in range(min(MAX_EXPORTS_TO_DUMP, the_module.exports.size())):
        # TODO NOTE: The intermediate value issue may be because a *new* instance is returned by `get_index()`. Need to cache or wrap something.
        var current_export = the_module.exports.get_index(index) # TODO: Figure out why intermediate variable needed to avoid crash..?
        print(" ")
        print("  export %d:" % index)
        print("      name: %s" % current_export.name)
        print("      type: %s (%s)" % [current_export.type, wasm.extern_kind_as_string(current_export.type)])

        match current_export.type:

            wasm.ExternKind.WASM_EXTERN_FUNC:

                var params_info = current_export._meta["params"]
                print("            params: %d" % params_info.size())
                for param_index in range(params_info.size()):
                    print("              param %d: %s" % [param_index, wasm.ValKindAsString[params_info[param_index]]])

                var results_info = current_export._meta["results"]
                print("            results: %d" % results_info.size())
                for result_index in range(results_info.size()):
                    print("             result %d: %s" % [result_index, wasm.ValKindAsString[results_info[result_index]]])


    print(" ")

    for index in range(min(MAX_IMPORTS_TO_DUMP, the_module.imports.size())):
        var current_import = the_module.imports.get_index(index) # TODO: Figure out why intermediate variable needed to avoid crash..?
        print(" ")
        print("  import %d:" % index)
        print("    name: %s" % current_import.name)
        print("    type: %s (%s)" % [current_import.type, wasm.extern_kind_as_string(current_import.type)])

        match current_import.type:

            wasm.ExternKind.WASM_EXTERN_FUNC:

                var params_info = current_import._meta["params"]
                print("            params: %d" % params_info.size())
                for param_index in range(params_info.size()):
                    print("              param %d: %s" % [param_index, wasm.ValKindAsString[params_info[param_index]]])

                var results_info = current_import._meta["results"]
                print("            results: %d" % results_info.size())
                for result_index in range(results_info.size()):
                    print("             result %d: %s" % [result_index, wasm.ValKindAsString[results_info[result_index]]])

    print(" ")


func _init():

    var simple_wasm = PoolByteArray([0x00, 0x61, 0x73, 0x6d, 0x01, 0x00, 0x00, 0x00, 0x01, 0x05, 0x01, 0x60,
        0x00, 0x01, 0x7f, 0x03, 0x02, 0x01, 0x00, 0x07, 0x05, 0x01, 0x01, 0x66,
        0x00, 0x00, 0x0a, 0x06, 0x01, 0x04, 0x00, 0x41, 0x04, 0x0b
        ])

    var wasm_engine = wasm.WasmEngine.new()

    var module = wasm_engine.load_wasm(simple_wasm)

    dump_info(module)


    var the_return_value = module.call_function(module._exported_functions.keys()[0]) # TODO: Handle properly. / Call by name.

    print(" ")
    prints("     the_return_value:", "0x%08x" % the_return_value)
    print(" ")


    var FILE_PATHS = [
      "wasm-examples/say-hello.wasm",
      "wasm-examples/params-and-rets.wasm",
    ]

    for file_path in FILE_PATHS:

        prints("File path:", file_path)
        print(" ")

        module = wasm_engine.load_wasm_from_file(file_path)
        dump_info(module)

        if file_path.get_file() in ["say-hello.wasm"]:

            for count in range(5):
                the_return_value = module.call_function(module._exported_functions.keys()[0]) # TODO: Handle properly.
                prints("     the_return_value:", "0x%08x" % the_return_value, ["",char(the_return_value)][int(the_return_value<0xff)])

            print(" ")

    print(" ")

    quit()
    return
