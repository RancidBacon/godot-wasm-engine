//
// Examples of parameter & return types.
//
// via <https://mbebenita.github.io/WasmExplorer/>
//
// Config: C99 -Os

#include <stdint.h>

int32_t ret_i() {
  return 1;
}

int32_t ret_i_in_i(int32_t i0) {
  return i0;
}

int64_t ret_i_in_ii(int32_t i0, int32_t i1) {
  return i0+i1;
}

float ret_f() {
  return 1.0;
}

float ret_f_in_f(float f0) {
  return f0;
}

float ret_f_in_ff(float f0, float f1) {
  return f0+f1;
}

float ret_f_in_fi(float f0, int32_t i0) {
  return f0*i0;
}
