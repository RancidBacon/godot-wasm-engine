## WASM Engine for Godot

### WebAssembly runtime add-on for Godot

 * Run functions from WebAssembly (`.wasm`) modules in your desktop Godot games & applications. [WIP*]

 * Enables opportunities to extend Godot functionality without per-platform compilation.

 * Potential to enable sandboxed customisation opportunities for your players/tool users. [WIP**]

 * Compile existing libraries to WASM for easier access to features they provide.

[WIP*] : Currently functions called must have no parameters and either return a 32-bit integer or nothing. Additionally, a module must not require any external imports. But--thanks to Foreigner--extending this current functionality only requires GDScript and no re-compilation!

[WIP**] : Wasmtime supports runtime engine configuration to limit executing functions but this functionality is not yet exposed by WASM Engine.

### Foundations

`godot-wasm-engine` is built on top of functionality provided by:

 * [Foreigner](https://github.com/and3rson/foreigner) -- [`libffi`](https://sourceware.org/libffi/) Foreign Function Interface wrapper for Godot (via [this WIP branch](https://github.com/follower/foreigner/tree/wip-poc-feature-buffer-struct)  which adds buffer/struct support.)

 * [`libwasmtime`](https://wasmtime.dev/)-- standards-compliant WebAssembly runtime implementation.

    The runtime being standards-compliant means it can potentially be replaced with other standards-compliant runtimes if required for your environment.


### Demonstration Projects

These two projects use WASM Engine to interact with `.wasm` files:

 * WASM Exploder: <https://gitlab.com/RancidBacon/wasm-exploder>

   Enables you to view the exports/imports of a `.wasm` file.

 * WebAssembly Calling Card (WACC) Viewer: <https://gitlab.com/RancidBacon/wacc-viewer>

   Displays dynamically generated graphics generated from WebAssembly in 2D & 3D.

### Get started

To use WASM Engine in your own projects you can copy the content of `src/addons/` into your Godot project `addons` directory.

Use of WASM Engine is currently under-documented: see the source of `src/wasmatter.gd` and the demonstration projects for how to load a `.wasm` file and execute a function.

The use of the [`package-all-with-version.py`](https://gitlab.com/RancidBacon/godot-foreigner-export-demo/-/blob/master/tools/package-all-with-version.py) tool is suggested to assist in exporting debug & release versions for each of the supported platforms (Linux, Mac, Windows).

----

Brought to you by RancidBacon.com
